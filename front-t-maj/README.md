# VueJs complete base
```
The project include these packages: 

VueRouter
Bootstrap
SweetAlert2
Axios
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
