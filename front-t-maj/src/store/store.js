import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        user: JSON.parse(localStorage.getItem('user')) || null
    },
    mutations: {
        change(state, user) {
            state.user = user;
        }
    },
    getters: {
        test: state => state.user
    }
});
