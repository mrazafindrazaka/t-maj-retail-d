'use strict';

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#bootstrap
 */

module.exports = () => {
  strapi.plugins['users-permissions'].services.user
    .add({ username: 'admin', email: 'admin@local.com', password: 'root54', roleType: 'admin', role: 3})
    .then(res => console.log('Admin is created'))
    .catch(error => console.log('Admin exist'));
};
