import VueRouter from "vue-router";

import Home from "../components/Home";
import SignUp from "../components/authentication/SignUp";
import SignIn from "../components/authentication/SignIn";
import Catalogue from "../components/Catalogue";
import Transactions from "../components/Transactions";
import TransactionForm from "../components/TransactionForm";

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/signup',
        name: 'signup',
        component: SignUp
    },
    {
        path: '/signin',
        name: 'signin',
        component: SignIn
    },
    {
        path: '/catalogue',
        name: 'catalogue',
        component: Catalogue
    },
    {
        path: '/transactions',
        name: 'transactions',
        component: Transactions
    },
    {
        path: '/transaction-form',
        name: 'transaction-form',
        component: TransactionForm
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;
