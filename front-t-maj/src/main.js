import Vue from 'vue';
import App from './App.vue';

import router from './config/router';
import { store } from "./store/store";
import './config/init';

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.mixin({
    data() {
        return {
            api_tmdb: 'https://api.themoviedb.org/3',
            api_tmdb_token: '?api_key=a9b06e550480e27d34e986e1054e1958',
            api_url: 'http://localhost:1337'
        }
    }
});

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
